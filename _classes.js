import { Input, Select } from './src/js/scripts/components.js';
import { authorization, getCard } from './src/js/requests/_requests.js';
import { VisitCardiologist, VisitDentist, VisitTherapist } from './src/js/scripts/visitClasses.js';

const allCards = [];
document.addEventListener('DOMContentLoaded', () => {
	showHide_loginForm();
	showHide_visitTypeDropdown();
	sendSelectedDoctorToClass();
});

const formContainer = document.querySelector('.login-container'); //селектор блока с формой авторизации, нужен для отображения
const formCreds = document.querySelector('.form-horizontal'); //селектор самой формы авторизации, нужен для того, что вытащить из формы данные в формате объекта
const btnSuccess = document.getElementById('button1id'); //айдишник кнопки подтверждения введенных в форму авторизации данных (конфирм)

const selectDoctor = document.querySelector('.choose-doctor'); //селектор тега селект с выбором врача
const doctorItem = document.querySelectorAll('.doctor-item'); //элементы тега селект с врачами, нужен для их перебора в цикле

const doctorBox = document.querySelector('.doctor-box'); //селектор блока модального окна для введения данных пациента при создании карточки, нужен для отображения
const doctorBoxData = document.querySelector('.doctor-box__data'); //селектор блка с полями ввода при создании карточки, нужен для того, чтобы в него апендить новые текстовые поля

const signIn = document.querySelector('#btn-enter'); //айдишник кнопки регистрации пользователя

const inputDoctor = document.querySelectorAll('.input-doctor'); //коллекция инпутов для введения пользователем информации о себе, нужен для того, чтобы потом вытащить введенные данные

const priorityItem = document.querySelectorAll('.priority-item');
const choosePriority = document.querySelector('.choose-priority');

const dropDown = document.querySelector('.dropdown');
document.querySelectorAll('.dropdown-item');

const btnClose = document.querySelector('#btnClose');

const searchInput = document.querySelector('#search-input');
const searchInputValue = document.querySelector('#search-input-value');

//функция отображения модалки с формой авторизации пользователя
function showHide_loginForm() {
	signIn.addEventListener('click', (event) => {
		event.preventDefault();
		if (!formContainer.classList.contains('db')) {
			formContainer.classList.add('db');
		} else {
			formContainer.classList.remove('db');
		}

		//при нажатии на конфирм делаем запрос на сервер с введенными кредами пользователя
		btnSuccess.addEventListener('click', (event) => {
			event.preventDefault();

			const textInput = document.querySelector('#textinput');
			const passwordInput = document.querySelector('#passwordinput');
			searchInputValue.addEventListener('change', function(event) {
				if (!this.value)
					allCards.forEach((item) => {
						if (item.cardElement.hasAttribute('hidden')) {
							item.cardElement.removeAttribute('hidden');
						}
					});
			});

			if (textInput.value !== '' && passwordInput.value !== '') {
				const creds = getAuthData(); //сохраняем результат выполнения функции, которая собирает введенные пользователем креды в формате объекта

				authorization(creds).then((result) => {
					if (!formContainer.classList.contains('db')) {
						formContainer.classList.add('db');
					} else {
						if (result.response) {
							formContainer.classList.remove('db');
							signIn.style.display = 'none';
							searchInput.style.display = 'flex';
							dropDown.style.display = 'block';
							const cardsField = document.querySelector('.cards');

							//Создаем и показываем лоадер
							const loading = document.createElement('h1');
							loading.classList.add('loader');
							loading.innerText = 'Загрузка карточек...';
							cardsField.append(loading);

							getCard(0, true).then((result) => {
								//Удаляем лоадер после загрузки карточек
								loading.remove();
								result.forEach((item) => {
									switch (item.doctor) {
										case 'Terapevt':
											{
												clearForm();
												const terapevtCard = new VisitTherapist(item);
												terapevtCard.showCard();
												terapevtCard.getId(item.id);
												allCards.push(terapevtCard);
											}
											break;
										case 'Cardiolog':
											{
												clearForm();
												const cardiologCard = new VisitCardiologist(item);
												cardiologCard.showCard();
												cardiologCard.getId(item.id);
												allCards.push(cardiologCard);
											}
											break;
										case 'Dentist':
											{
												clearForm();
												const dentistCard = new VisitDentist(item);
												dentistCard.showCard();
												dentistCard.getId(item.id);
												allCards.push(dentistCard);
											}
											break;
									}
								});
							});
							function clearForm() {
								const form = document.querySelector('.doctor-box__data');
								// console.log(form.children);
								// for (const key of form) {
								// 	console.log(key, form[key]);
								// }
							}
						} else {
							const error = document.querySelector('.help-block');
							error.innerText = result.error;
							error.style.color = 'red';
						}
					}
				});

				searchCard(); //функция фильтра карточек
			} else {
				if (textInput.value === '') {
					textInput.style.borderColor = 'red';
				}
				if (passwordInput.value === '') {
					passwordInput.style.borderColor = 'red';
				}
			}
		});
	});
}

function showHide_visitTypeDropdown() {
	const chooseVisitTypeDropdown = document.querySelector('#dropdownVisitMenuButton');
	chooseVisitTypeDropdown.innerHTML = 'Create card';
	chooseVisitTypeDropdown.addEventListener('click', () => {
		const testBtn = document.querySelector('.showHideInfo');

		const visitTypeDropdownMenu = document.querySelector('.visitDropdown');

		if (visitTypeDropdownMenu.classList.contains('db')) {
			visitTypeDropdownMenu.classList.remove('db');
			chooseVisitTypeDropdown.innerHTML = 'Create card';
		} else {
			visitTypeDropdownMenu.classList.add('db');
			chooseVisitTypeDropdown.innerHTML = 'Pick the doctor';
		}

		deleteElem();
	});
}

//функция, которая проходит по коллекции инпутов в форме создания карточки и записывает значения в созданный объект
function getInputDoctorData() {
	const inputDataPacient = {};

	for (let i = 0; i < inputDoctor.length; i++) {
		inputDataPacient[i] = inputDoctor[i].value;
	}

	return inputDataPacient;
}

//создаёт экземпляр класса инпут и вызывает там функцию, которую помещает все данные из инпутов в объект
function sendInputDataToClass() {
	const testInput = getInputDoctorData();

	return new Input(testInput).checkInputValue();
}

//функция, которая берёт креды, введенные пользователем в форме авторизации и возвращает объект с их значениями
const getAuthData = () => {
	const authData = new FormData(formCreds);

	return {
		email: authData.get('textinput'),
		password: authData.get('passwordinput')
	};
};

function sendSelectedDoctorToClass() {
	const mySelect = new Select().chooseDoctor();
	return mySelect;
}

//функция, которая проходит по коллекции тега селект с приоритеностью и записывает значения в созданный объект
function getSelectedPriorityData() {
	const selectedPriorityObj = {};

	for (let i = 1; i < priorityItem.length; i++) {
		selectedPriorityObj[i] = priorityItem[i].value;
	}

	return selectedPriorityObj;
}

//функция, которая создаёт экземпляр класса селект с объектом с данными полученными из тега селект с приоритетностью
function sendSelectedPriorityToClass(priority) {
	const prioritySelect = new Select(priority).selectPriority();
	return prioritySelect;
}

//коллбэк, который срабатывает при изменении тега селект (при выборе приоритетности)
choosePriority.onchange = () => {
	const priority = getSelectedPriorityData();
	sendSelectedPriorityToClass(priority);
};

//функция создания элемента кнопки в форме заполнения данных пользователем
function createButton_elem() {
	const createCard = document.createElement('button');
	createCard.type = 'button';
	createCard.classList.add('btn');
	createCard.classList.add('btn-primary');
	createCard.innerText = 'Create';
	createCard.style.width = '50%';
	createCard.style.marginLeft = '25%';

	return createCard;
}

//функция создания стиля полям ввода в форме заполнения данных пациентом
function addClassStyle(...textArea) {
	textArea.forEach((itemField) => {
		itemField.classList.add('form-control');
	});
}
function searchCard() {
	//при изменении инпута, делает запрос на сервер для получения всех карточек и парсит массив по введенным значениям
	// searchInputValue.onchange = () => {
	// 	getCard(null, true, parseDataCards);
	// }

	const searchBtn = document.getElementById('button-addon2');
	searchInputValue.addEventListener('input', (event) => {
		allCards.forEach((card) => {
			if (searchInputValue.value == '') {
				card.cardElement.hidden = false;
			} else {
				search(card);
			}
		});
	});
}
function search(input) {
	for (const key in input) {
		if (input.hasOwnProperty(key)) {
			const element = input[key];

			if (typeof searchInputValue.value == 'string') {
				if (searchInputValue.value.includes(element)) {
					input.cardElement.hidden = false;
					return;
				} else {
					input.cardElement.hidden = true;
				}
			}
		}
	}
}

function parseDataCards(arrayCards) {
	const parseDataSearch = arrayCards
		.filter((itemCardObj) => {
			return itemCardObj.fio; //возвращает те объекты где есть свойство фио
		})
		.forEach((elemOfObj) => {
			//проверяет есть ли в свойствах объекта значения, которое ввел пользователь, если есть совпадения, отрисовывает его на страницу в тег р и аппендит его в дивак
			if (elemOfObj.fio.includes(searchInputValue.value)) {
				const searchElemBlock = document.createElement('div');
				for (let searchItem in elemOfObj) {
					if (searchItem === 'fio') {
						const pacientProp = searchItem.replace('fio', 'Пациент'); //меняем название свойства объекта
						const searchElemText = document.createElement('p');
						searchElemText.innerText = `${pacientProp}: ${elemOfObj[searchItem]}`;
						searchElemBlock.append(searchElemText);
						document.body.append(searchElemBlock);
					}

					if (searchItem === 'age') {
						const pacientProp = searchItem.replace('age', 'Возраст');
						const searchElemText = document.createElement('p');
						searchElemText.innerText = `${pacientProp}: ${elemOfObj[searchItem]}`;
						searchElemBlock.append(searchElemText);
						document.body.append(searchElemBlock);
					}

					if (searchItem === 'doctor') {
						const pacientProp = searchItem.replace('doctor', 'Лечащий врач');
						const searchElemText = document.createElement('p');
						searchElemText.innerText = `${pacientProp}: ${elemOfObj[searchItem]}`;
						searchElemBlock.append(searchElemText);
						document.body.append(searchElemBlock);
					}

					if (searchItem === 'priority') {
						const pacientProp = searchItem.replace('priority', 'Срочность вызова');
						const searchElemText = document.createElement('p');
						searchElemText.innerText = `${pacientProp}: ${elemOfObj[searchItem]}`;
						searchElemBlock.append(searchElemText);
						document.body.append(searchElemBlock);
					}

					if (searchItem === 'aimVisit') {
						const pacientProp = searchItem.replace('aimVisit', 'Цель визита');
						const searchElemText = document.createElement('p');
						searchElemText.innerText = `${pacientProp}: ${elemOfObj[searchItem]}`;
						searchElemBlock.append(searchElemText);
						document.body.append(searchElemBlock);
					}

					if (searchItem === 'descriptionVisit') {
						const pacientProp = searchItem.replace('descriptionVisit', 'Краткое описание');
						const searchElemText = document.createElement('p');
						searchElemText.innerText = `${pacientProp}: ${elemOfObj[searchItem]}`;
						searchElemBlock.append(searchElemText);
						document.body.append(searchElemBlock);
					}

					if (searchItem === 'pressure') {
						const pacientProp = searchItem.replace('pressure', 'Обычное давление');
						const searchElemText = document.createElement('p');
						searchElemText.innerText = `${pacientProp}: ${elemOfObj[searchItem]}`;
						searchElemBlock.append(searchElemText);
						document.body.append(searchElemBlock);
					}

					if (searchItem === 'massIndex') {
						const pacientProp = searchItem.replace('massIndex', 'Индекс массы тела');
						const searchElemText = document.createElement('p');
						searchElemText.innerText = `${pacientProp}: ${elemOfObj[searchItem]}`;
						searchElemBlock.append(searchElemText);
						document.body.append(searchElemBlock);
					}

					if (searchItem === 'heartAche') {
						const pacientProp = searchItem.replace('heartAche', 'Перенесённые сердечно-сосудистые заболевания');
						const searchElemText = document.createElement('p');
						searchElemText.innerText = `${pacientProp}: ${elemOfObj[searchItem]}`;
						searchElemBlock.append(searchElemText);
						document.body.append(searchElemBlock);
					}

					if (searchItem === 'lastVisit') {
						const pacientProp = searchItem.replace('lastVisit', 'Последний визит');
						const searchElemText = document.createElement('p');
						searchElemText.innerText = `${pacientProp}: ${elemOfObj[searchItem]}`;
						searchElemBlock.append(searchElemText);
						document.body.append(searchElemBlock);
					}

					if (searchItem === 'hasProthesic') {
						const pacientProp = searchItem.replace('hasProthesic', 'Протезы/коронки');
						const searchElemText = document.createElement('p');
						searchElemText.innerText = `${pacientProp}: ${elemOfObj[searchItem]}`;
						searchElemBlock.append(searchElemText);
						document.body.append(searchElemBlock);
					}
				}
			}
		});
}

function deleteElem(...arrHtmlTags) {
	const clearFio = document.querySelector('#fioInput');
	const clearAge = document.querySelector('#ageInput');

	clearFio.style.borderColor = 'none';
	clearAge.style.borderColor = 'none';

	arrHtmlTags.forEach((itemToDelete) => {
		if (itemToDelete === null) return false;
		else itemToDelete.remove();
	});
}

//три объекта, в которые помещаются значения из инпутов, селекта и текстареа(потом они клонируются в один объект)
const allInputValue = {};
const allSelectValue = {};
const allTextAreaValue = {};

export {
	formContainer,
	formCreds,
	showHide_loginForm,
	getAuthData,
	sendSelectedDoctorToClass,
	doctorItem,
	selectDoctor,
	getInputDoctorData,
	doctorBoxData,
	sendSelectedPriorityToClass,
	sendInputDataToClass,
	choosePriority,
	allInputValue,
	allSelectValue,
	allTextAreaValue,
	doctorBox,
	dropDown,
	inputDoctor,
	createButton_elem,
	addClassStyle,
	btnClose,
	parseDataCards,
	searchInput,
	searchInputValue,
	searchCard,
	allCards,
	deleteElem
};
