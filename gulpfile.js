// require all packages
const gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    autoPrefixer = require('gulp-autoprefixer'),
    imageMin = require('gulp-imagemin'),
    size = require('gulp-size'),
    cache = require('gulp-cache'),
    del = require('del'),
    uglify = require('gulp-uglify'),
    purgecss = require('gulp-purgecss'),
    browserSync = require('browser-sync').create();

//gulp-task for transform default SCSS styles to CSS, clean useless styles, put prefixes for another browsers and move it to the dist
gulp.task('moveMainCss', function() {
    return (gulp
        .src([ 'src/css/reset.css', 'src/sass/styles/default/main-styles.scss' ])
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles.min.css'))
        .pipe(purgecss({ content: [ './index.html' ] }))
        .pipe(autoPrefixer([ 'last 15 versions', '> 1%', 'ie 8', 'ie 7' ], { cascade: true }))
        // .pipe(minifyCSS())
        .pipe(gulp.dest('dist/css')) );
});

//gulp-task for transform medium SCSS styles to CSS, clean useless styles, put prefixes for another browsers and move it to the dist
gulp.task('moveMdCss', function() {
    return (gulp
        .src('src/sass/styles/medium/screen-medium.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles-md.css'))
        .pipe(purgecss({ content: [ './index.html' ] }))
        .pipe(autoPrefixer([ 'last 15 versions', '> 1%', 'ie 8', 'ie 7' ], { cascade: true }))
        // .pipe(minifyCSS())
        .pipe(gulp.dest('dist/css')) );
});

//gulp-task for transform large SCSS styles to CSS, clean useless styles, put prefixes for another browsers and move it to the dist
gulp.task('moveLgCss', function() {
    return (gulp
        .src('src/sass/styles/large/screen-large.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles-lg.css'))
        .pipe(purgecss({ content: [ './index.html' ] }))
        .pipe(autoPrefixer([ 'last 15 versions', '> 1%', 'ie 8', 'ie 7' ], { cascade: true }))
        // .pipe(minifyCSS())
        .pipe(gulp.dest('dist/css')) );
});

//gulp-task for watch changes in all SCSS files from src and do transformation to CSS and move them to the dist
gulp.task('watchCSS', function() {
    return gulp.watch('src/sass/**/*.scss', gulp.series('moveMainCss', 'moveMdCss', 'moveLgCss'));
});

//gulp-task for transform default, medium, large SCSS styles to CSS, clean useless styles, put prefixes for another browsers and move it to the dist
gulp.task('moveCSS', gulp.series('moveMainCss', 'moveMdCss', 'moveLgCss'));

//gulp-task to minimize images and move them to the dist
gulp.task('imagesMin', function() {
    return gulp
        .src('src/assets/**/*.png')
        .pipe(
            cache(
                imageMin({
                    optimizationLevel: 5,
                    progressive: true,
                    interlaced: true
                })
            )
        )
        .pipe(size({ title: 'size of images' }))
        .pipe(gulp.dest('dist/images'));
});

//gulp-task to minimize JS file from src and move it to the dits
gulp.task('jsMin', function() {
    return gulp
        .src('src/js/**/*.js')
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(size({ title: 'size of custom js' }))
        .pipe(gulp.dest('dist/js'));
});

//gulp-task for clean dist folder
gulp.task('clean-dist', function(done) {
    del.sync('dist');
    done();
});

//gulp-task for build dist folder (clean dist folder, create CSS files, minimize images, minimize JS files)
gulp.task('build', gulp.series('clean-dist', 'moveCSS', 'imagesMin', 'jsMin'));

//gulp-task for open live server with watching SCSS files, JS files and HTML page
gulp.task('server', function(done) {
    browserSync.init({
        server: '../stepproject_cards/',
        notify: false
    });
    gulp
        .watch([ './src/sass/**/*.scss', './src/js/**/*.js', './index.html' ], gulp.series('moveCSS', 'jsMin'))
        .on('change', () => {
            browserSync.reload();
            done();
        });
    done();
});

//gulp-task for transform SCSS files, move them to dist/css and open live server with watching SCSS files, JS files and HTML page
gulp.task('dev', gulp.series('moveCSS', 'server'));

