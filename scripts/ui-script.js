// window.onload = () => {
// };

document.addEventListener('DOMContentLoaded', () => {
	// showHide_loginForm();
	document.querySelector('.create-visit-form__form').setAttribute('hidden', '');
	//TODO attach event when elenemt is created
	showHide_visitTypeDropdown();
	// showHideVisitForm();

	const cardiolog = document.querySelector('#cardiolog');
	const dentist = document.querySelector('#dentist');
	const terapevt = document.querySelector('#terapevt');

	cardiolog.addEventListener('click', () => renderVisitCardiologForm());
	dentist.addEventListener('click', () => renderVisitDentistForm());
	terapevt.addEventListener('click', () => renderVisitTerapevtForm());
});

//TODO bind to actual DOM element
function showHide_visitTypeDropdown() {
	const chooseVisitTypeDropdown = document.querySelector('#dropdownVisitMenuButton');
	chooseVisitTypeDropdown.addEventListener('click', () => {
		const visitTypeDropdownMenu = document.querySelector('.visitDropdown');
		console.log(visitTypeDropdownMenu);

		if (visitTypeDropdownMenu.classList.contains('db')) {
			visitTypeDropdownMenu.classList.remove('db');
			chooseVisitTypeDropdown.innerHTML = 'Create card';
		} else {
			visitTypeDropdownMenu.classList.add('db');
			chooseVisitTypeDropdown.innerHTML = 'Pick the doctor';
		}
	});
}

function showHide_loginForm() {
	const signIn = document.querySelector('#btn-enter');
	signIn.addEventListener('click', () => {
		const formContainer = document.querySelector('.login-container');

		console.log(formContainer);
		if (formContainer.classList.contains('db')) {
			formContainer.classList.remove('db');
		} else {
			formContainer.classList.add('db');
		}
	});
}

//TODO fill methods to the fields
const functionObject = {
	// fio: renderFioField(''),
	// age: renderAgeField(),
	doctor: 'ivanova doctor',
	aimVisit: 'aimVisit',
	descVisit: 'decsroption of visit',
	status: 'statusofVisit',
	emergency: 'emergencyOfVisit'
};

const obj = {
	lastName: 'tulieakov',
	firstName: 'viacheslav',
	fatherName: 'ruslanovich',
	age: 25,
	doctor: 'ivanova doctor',
	aimVisit: 'aimVisit',
	descVisit: 'decsroption of visit',
	status: 'statusofVisit',
	emergency: 'emergencyOfVisit'
};

// renderVisitCardiologForm();
function renderVisitCardiologForm() {
	let form = document.querySelector('.create-visit-form__form');
	form.innerHTML = '';
	form.removeAttribute('hidden');

	renderSubmit(form);
	renderLastVisitDate(form);
	renderNormalPressureAndWeightIndex(form);
	renderGoalOfVisit(form);
	renderPastDeseases(form);
	renderShortDescription(form);
	renderAgeAndNecessaryField(form, true);
	renderFioField(form);
}
// renderVisitDentistForm();
function renderVisitDentistForm() {
	let form = document.querySelector('.create-visit-form__form');
	form.innerHTML = '';
	form.removeAttribute('hidden');

	renderSubmit(form);
	renderLastVisitDate(form);
	renderShortDescription(form);
	renderGoalOfVisit(form);
	renderNecessaryField(renderFormRow(form));
	renderFioField(form);
}

// renderVisitTerapevtForm();
function renderVisitTerapevtForm() {
	let form = document.querySelector('.create-visit-form__form');
	form.innerHTML = '';
	form.removeAttribute('hidden');

	renderSubmit(form);
	renderLastVisitDate(form);
	renderShortDescription(form);
	renderGoalOfVisit(form);
	renderAgeAndNecessaryField(form, false);
	renderFioField(form);
}

function renderFormRow(form) {
	const formRow = document.createElement('div');
	formRow.classList.add('form-row');
	form.prepend(formRow);
	return formRow;
}

function renderFioField(form) {
	const formRow = document.createElement('div');
	formRow.classList.add('form-row');

	const divCol = document.createElement('div');
	divCol.classList.add('col-md-12');
	divCol.classList.add('mb-3');

	const label = document.createElement('label');
	label.setAttribute('for', 'fio');
	label.innerText = 'ФИО';

	const input = document.createElement('input');
	input.classList.add('form-control');
	input.setAttribute('type', 'text');
	input.setAttribute('id', 'fio');

	divCol.append(label);
	divCol.append(input);
	formRow.append(divCol);
	console.log(formRow);

	form.prepend(formRow);
}

function renderAgeAndNecessaryField(form, renderNecessary) {
	// debugger;
	const formRow = document.createElement('div');
	formRow.classList.add('form-row');

	const divCol = document.createElement('div');
	divCol.classList.add('col-md-6');
	divCol.classList.add('mb-3');

	const label = document.createElement('label');
	label.setAttribute('for', 'age');
	label.innerText = 'Возраст';

	const input = document.createElement('input');
	input.classList.add('form-control');
	input.setAttribute('type', 'text');
	input.setAttribute('id', 'age');

	if (renderNecessary) renderNecessaryField(formRow);

	divCol.append(label);
	divCol.append(input);
	formRow.append(divCol);
	form.prepend(formRow);
}
function renderNecessaryField(formRow) {
	const divCol = document.createElement('div');
	divCol.classList.add('col-md-6');
	divCol.classList.add('mb-3');

	const label = document.createElement('label');
	label.setAttribute('for', 'necessary');
	label.innerText = 'Срочность визита';

	const select = document.createElement('select');
	select.classList.add('custom-select');
	select.setAttribute('id', 'necessary');

	const optionDefault = document.createElement('option');
	const optionNormal = document.createElement('option');
	const optionPriority = document.createElement('option');
	const optionImmediatly = document.createElement('option');

	optionDefault.setAttribute('selected', 'selected');
	optionDefault.setAttribute('disabled', 'disabled');
	optionDefault.innerText = 'Выбор...';
	optionNormal.innerText = 'Обычная';
	optionPriority.innerText = 'Приоритетная';
	optionImmediatly.innerText = 'Неотложная';

	divCol.append(label);
	select.append(optionDefault, optionNormal, optionPriority, optionImmediatly);
	divCol.append(select);

	formRow.prepend(divCol);
}

function renderGoalOfVisit(form) {
	const formRow = document.createElement('div');
	formRow.classList.add('form-row');

	const divCol = document.createElement('div');
	divCol.classList.add('col-md-12');
	divCol.classList.add('mb-3');

	const label = document.createElement('label');
	label.setAttribute('for', 'necessary');
	label.innerText = 'Цель визита';

	const input = document.createElement('input');
	input.classList.add('form-control');
	input.setAttribute('type', 'text');
	input.setAttribute('id', 'goal');

	divCol.append(label, input);
	formRow.append(divCol);
	form.prepend(formRow);
}

function renderShortDescription(form) {
	const formRow = document.createElement('div');
	formRow.classList.add('form-row');

	const divCol = document.createElement('div');
	divCol.classList.add('input-group');
	divCol.classList.add('col-md-12');
	divCol.classList.add('mb-3');

	const inputGroup = document.createElement('div');
	inputGroup.classList.add('input-group-prepend');

	const span = document.createElement('span');
	span.classList.add('input-group-text');
	span.innerHTML = 'Краткое описание<br>визита';

	const textArea = document.createElement('textarea');
	textArea.classList.add('form-control');
	textArea.setAttribute('area-label', 'With textarea');

	inputGroup.append(span);
	divCol.append(inputGroup);
	divCol.append(textArea);
	formRow.append(divCol);
	form.prepend(formRow);
}

function renderPastDeseases(form) {
	const formRow = document.createElement('div');
	formRow.classList.add('form-row');

	const divCol = document.createElement('div');
	divCol.classList.add('input-group');
	divCol.classList.add('col-md-12');
	divCol.classList.add('mb-3');

	const inputGroup = document.createElement('div');
	inputGroup.classList.add('input-group-prepend');

	const span = document.createElement('span');
	span.classList.add('input-group-text');
	span.innerHTML = 'Перенесенные заболевания<br>сердечно-сосудистой системы';

	const textArea = document.createElement('textarea');
	textArea.classList.add('form-control');
	textArea.setAttribute('area-label', 'With textarea');

	inputGroup.append(span);
	divCol.append(inputGroup);
	divCol.append(textArea);
	formRow.append(divCol);
	form.prepend(formRow);
}

function renderNormalPressureAndWeightIndex(form) {
	const formRow = document.createElement('div');
	formRow.classList.add('form-row');

	const divCol = document.createElement('div');
	divCol.classList.add('col-md-6');
	divCol.classList.add('mb-3');

	const label = document.createElement('label');
	label.setAttribute('for', 'normal-pressure');
	label.innerText = 'Обычное давление';

	const input = document.createElement('input');
	input.setAttribute('type', 'text');
	input.setAttribute('id', 'normal-pressure');
	input.classList.add('form-control');

	renderWeightIndex(formRow);
	divCol.append(label);
	divCol.append(input);
	formRow.append(divCol);
	form.prepend(formRow);
}

function renderWeightIndex(form) {
	const divCol = document.createElement('div');
	divCol.classList.add('col-md-6');
	divCol.classList.add('mb-3');

	const label = document.createElement('label');
	label.setAttribute('for', 'weight-index');
	label.innerText = 'Индекс массы тела';

	const input = document.createElement('input');
	input.setAttribute('type', 'text');
	input.setAttribute('id', 'weight-index');
	input.classList.add('form-control');

	divCol.append(label, input);
	form.prepend(divCol);
}

function renderLastVisitDate(form) {
	const formGroup = document.createElement('div');
	formGroup.classList.add('form-group');

	// const divCol = document.createElement('div');
	// divCol.classList.add('col-md-6');
	// divCol.classList.add('mb-3');

	const label = document.createElement('label');
	label.setAttribute('for', 'inputDate');
	label.innerHTML = 'Выберите дату последнего визита:';

	const input = document.createElement('input');
	input.setAttribute('type', 'date');
	input.classList.add('form-control');

	formGroup.append(label, input);
	form.prepend(formGroup);
}
function renderSubmit(form) {
	const button = document.createElement('button');
	button.classList.add('btn');
	button.classList.add('btn-primary');
	button.setAttribute('type', 'submit');
	button.innerText = 'Submit form';
	form.prepend(button);
}
