import axios from "./axios.js";
// import {therapistForm} from "../scripts/visitForms";
import {searchCard} from "../../../_classes.js";

const userObj = {
    email: 'andrey.shkurenko@icloud.com',
    password: 'sefcY6-fahgyk-sefzur'
}

async function authorization(obj) {

    try {
        //Check for function param
        if (!obj) throw new Error('Params not defined');

        //Request with ready response
        const response = await axios.post('/login', obj);
        const responseData = response.data;

        //Check for success request
        if (!responseData) throw new Error('Request error');

        //Check for success information
        if (responseData.status === 'Error') throw new Error(responseData.message);

        //Initializing the access token
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + responseData.token;
        return {response:true};

    } catch (error) {
        //Catching error
        return {response:false, error: error.message};
    } finally {

        //Make something in the end
    }
}


async function createCard(card) {
    try {
        if (!card) throw new Error('Передайте карточку');

        //Request for create a card with needed params
        const request = await axios.post('/cards', card);
        //Check for successful request
        if (request.status === 200) return {response: true, data: request.data};

    }catch (error){
        return {response:false, error: error.message};
    }
}


async function getCard(id, allCards = false, parseDataCards){
    if (allCards){
        try{
        //If we need all cards
        const response = await axios.get('/cards')
            //Check for successful request
        if (response.status === 200) return response.data;

        }catch(error){
            console.log(error);
        }
    }else{

        //If we need a specific card
        axios.get(`/cards${id}`)
            .then(response => {

                //Check for successful request
                if (response.status === 200) {

                    //Check for existence of the card
                    if (response.data === null) throw new Error('Card is exist in database !');

                    //Show card on the UI
                }
            })
            .catch(error => console.log(error));
    }
}


async function changeCard(id, newCard){
    try {
        if (!id) throw new Error('Передайте айди карточки');

        //Request for change something in the card=
        const response = await axios.put(`/cards/${id}`, newCard);

        if(response.status === 200) return {response: true};

    }catch (error){
        return {response:false, error: error.message};
    }

}

async function deleteCard(card){
    try {
        if (!card) throw new Error('Передайте карточку');

        //Request for delete card
        const response = await axios.delete(`/cards/${card.id}`)

        //If all is ok -> delete this card from the UI
        if (response.data.status === 'Success') return {response: true};
    }catch (error){
        return {response:false, error: error.message};
    }

}

export {authorization, getCard, createCard, changeCard, deleteCard};

