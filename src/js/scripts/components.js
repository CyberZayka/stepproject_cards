import {
	doctorBoxData,
	sendInputDataToClass,
	choosePriority,
	allInputValue,
	allSelectValue,
	allTextAreaValue,
	doctorBox,
	dropDown,
	createButton_elem,
	addClassStyle,
	btnClose,
	searchInput,
	deleteElem
} from "../../../_classes.js"
import {Form, cardiologistForm, dentistForm, therapistForm} from "./visitForms.js";

//import axios from "../requests/axios.js";
import { authorization, getCard, createCard, changeCard, deleteCard } from '../requests/_requests.js';

//шаблон элементов поля ввода (инпут)
class Input {
	constructor(inputObj) {
		//свойства инпутов при создании карточки
		this.fio = inputObj[0];
		this.age = inputObj[1];
	}

	//если все поля инпутов заполнены, засовываем введённые значения в объект для всех инпутов
	checkInputValue() {
		if (this.fio && this.age) {
			allInputValue.fio = this.fio;
			allInputValue.age = this.age;
		}
		return allInputValue;
	}
}

//шаблон элементов выпадающие списка (селект)
class Select {
	constructor() {}

    chooseDoctor() {

    	const hideDrop = document.querySelector('#dropdownVisitMenuButton');

        const cardiolog = document.querySelector('#cardiolog');
        const dentist = document.querySelector('#dentist');
        const therapist = document.querySelector('#terapevt');

        cardiolog.addEventListener('click', () => {
            this.checkCardio();
            allSelectValue.doctor = cardiolog.innerHTML;
            hideDrop.click();
			const dropDownBtn = document.querySelector('#dropdownVisitMenuButton');
			dropDownBtn.disabled = true;

        });
        dentist.addEventListener('click', () => {
            this.checkDentist();
            allSelectValue.doctor = dentist.innerHTML;
			hideDrop.click();
			const dropDownBtn = document.querySelector('#dropdownVisitMenuButton');
			dropDownBtn.disabled = true;

        });
        therapist.addEventListener('click', () => {
            this.checkTherapio();
            allSelectValue.doctor = therapist.innerHTML;
			hideDrop.click();
			const dropDownBtn = document.querySelector('#dropdownVisitMenuButton');
			dropDownBtn.disabled = true;
        });

    }


	//функция, создающая экземпляр класса текстареа, который добавляет поля ввода
	//текстареа для кардиолога
	checkCardio() {
		const myCardio = new TextArea().cardioText();
		return myCardio;
	}

	//функция, создающая экземпляр класса текстареа, который добавляет поля ввода
	//текстареа для дантиста
	checkDentist() {
		const myDentist = new TextArea().dentistText();
		return myDentist;
	}

	//функция, создающая экземпляр класса текстареа, который добавляет поля ввода
	//текстареа для терапевта
	checkTherapio() {
		const myTherapist = new TextArea().TherapioText();
		return myTherapist;
	}

	//выбранное значение приоритета в выпадающем списке засовываем в объект для всех тегов селект
	selectPriority() {
		allSelectValue.priority = choosePriority.value;
	}
}

//шаблон элемента текстового поля, которое появляется в зависимости от выбора врача
//(текстареа)
class TextArea {
	constructor() {
	}


	//функция которая создает текстовые поля для терапевта
	TherapioText() {
		doctorBox.style.display = 'block';

		const contentCard = document.querySelector('.content-card');
		contentCard.style.width = '100%';

		const aimVisit = document.createElement('textarea');
		const descriptionVisit = document.createElement('textarea');

		aimVisit.id = 'aimId';
		descriptionVisit.id = 'descId';

		aimVisit.placeholder = 'Цель визита:';
		descriptionVisit.placeholder = 'Краткое описание:';

		addClassStyle(aimVisit, descriptionVisit);

		const createCard = createButton_elem();
		createCard.id = 'forDelete';

		doctorBoxData.append(aimVisit, descriptionVisit, createCard);

		createCard.addEventListener('click', (event) => {
			event.preventDefault();

			const fioInput = document.querySelector('#fioInput');
			const ageInput = document.querySelector('#ageInput');

			const aimText = document.querySelector('#aimId');
			const descText = document.querySelector('#descId');

			if (
				fioInput.value !== '' &&
				ageInput.value !== '' &&
				aimText.value !== '' &&
				descText.value !== '' &&
				choosePriority.value !== 'choose a priority'
			) {
				sendInputDataToClass(); //создаёт экземпляр класса инпут с оъектом введенных значений

				doctorBox.style.display = 'none';

				allTextAreaValue.aimVisit = aimVisit.value;
				allTextAreaValue.descriptionVisit = descriptionVisit.value;

				const createForm = new therapistForm(allInputValue, allSelectValue, allTextAreaValue);
				deleteElem(aimText, descText, createCard);

				const dropDownBtn = document.querySelector('#dropdownVisitMenuButton');
				dropDownBtn.disabled = false;
				//return createForm;

			} else {
				if (fioInput.value === '') {
					fioInput.style.borderColor = 'red';
				}
				if (ageInput.value === '') {
					ageInput.style.borderColor = 'red';
				}
				if (aimText.value === '') {
					aimText.style.borderColor = 'red';
				}
				if (descText.value === '') {
					descText.style.borderColor = 'red';
				}
				if (choosePriority.value === 'choose a priority') {
					alert('Choose priority of order please');
				}
			}
		});

		btnClose.addEventListener('click', (event) => {
			event.preventDefault();

			doctorBox.style.display = 'none';
			searchInput.style.display = 'flex';
			dropDown.style.display = 'block';

			const aimText = document.querySelector('#aimId');
			const descText = document.querySelector('#descId');
			const deleteBtn = document.querySelector('#forDelete');

			deleteElem(aimText, descText, createCard);

			const dropDownBtn = document.querySelector('#dropdownVisitMenuButton');
			dropDownBtn.disabled = false;
		})


	}


	//функция, которая создаёт текстовые поля для заполнения, если выбран кардиолог
	cardioText() {
		doctorBox.style.display = 'block';

		const aimVisit = document.createElement('textarea');
		const descriptionVisit = document.createElement('textarea');

		const pressure = document.createElement('textarea');
		const massIndex = document.createElement('textarea');
		const heartAche = document.createElement('textarea');

		const contentCard = document.querySelector('.content-card');
		contentCard.style.width = '100%';

		aimVisit.id = 'aimId';
		descriptionVisit.id = 'descId';
		pressure.id = 'pressId';
		massIndex.id = 'massId';
		heartAche.id = 'heartId';

		aimVisit.placeholder = 'Цель визита:';
		descriptionVisit.placeholder = 'Краткое описание:';
		pressure.placeholder = 'Обычное давление:';
		massIndex.placeholder = 'Индекс массы тела:';
		heartAche.placeholder = 'Перенесённые заболевания сердечно-сосудистой системы:';

		addClassStyle(aimVisit, descriptionVisit, pressure, massIndex, heartAche);

		const createCard = createButton_elem();
		createCard.id = 'forDelete';

		doctorBoxData.append(aimVisit, descriptionVisit, heartAche, massIndex, pressure, createCard);

		createCard.addEventListener('click', (event) => {
			event.preventDefault();

			const fioInput = document.querySelector('#fioInput');
			const ageInput = document.querySelector('#ageInput');

			const aimText = document.querySelector('#aimId');
			const descText = document.querySelector('#descId');
			const pressText = document.querySelector('#pressId');
			const massText = document.querySelector('#massId');
			const heartText = document.querySelector('#heartId');

			if (
				fioInput.value !== '' &&
				ageInput.value !== '' &&
				aimText.value !== '' &&
				descText.value !== '' &&
				pressText.value !== '' &&
				massText.value !== '' &&
				heartText.value !== '' &&
				choosePriority.value !== 'choose a priority'
			) {
				sendInputDataToClass(); //создаёт экземпляр класса инпут с оъектом введенных значений
				doctorBox.style.display = 'none';

				allTextAreaValue.aimVisit = aimVisit.value;
				allTextAreaValue.descriptionVisit = descriptionVisit.value;
				allTextAreaValue.pressure = pressure.value;
				allTextAreaValue.massIndex = massIndex.value;
				allTextAreaValue.heartAche = heartAche.value;

				const createForm = new cardiologistForm(allInputValue, allSelectValue, allTextAreaValue);

				deleteElem( aimText, descText, pressText, massText, heartText, createCard);

				const dropDownBtn = document.querySelector('#dropdownVisitMenuButton');
				dropDownBtn.disabled = false;
				//return createForm;
			} else {
				if (fioInput.value === '') {
					fioInput.style.borderColor = 'red';
				}
				if (ageInput.value === '') {
					ageInput.style.borderColor = 'red';
				}
				if (aimText.value === '') {
					aimText.style.borderColor = 'red';
				}
				if (descText.value === '') {
					descText.style.borderColor = 'red';
				}
				if (pressText.value === '') {
					pressText.style.borderColor = 'red';
				}
				if (massText.value === '') {
					massText.style.borderColor = 'red';
				}
				if (heartText.value === '') {
					heartText.style.borderColor = 'red';
				}
				if (choosePriority.value === 'choose a priority') {
					alert('Choose priority of order please');
				}
			}
		});

		btnClose.addEventListener('click', (event) => {
			event.preventDefault();

			doctorBox.style.display = 'none';
			searchInput.style.display = 'flex';
			dropDown.style.display = 'block';

			const aimText = document.querySelector('#aimId');
			const descText = document.querySelector('#descId');
			const pressText = document.querySelector('#pressId');
			const massText = document.querySelector('#massId');
			const heartText = document.querySelector('#heartId');
			const deleteBtn = document.querySelector('#forDelete');

			deleteElem( aimText, descText, pressText, massText, heartText, createCard);

			const dropDownBtn = document.querySelector('#dropdownVisitMenuButton');
			dropDownBtn.disabled = false;
		})

	}


	//функция, которая создаёт текстовые поля для заполнения, если выбран дантист

	dentistText() {

		doctorBox.style.display = 'block';

		const contentCard = document.querySelector('.content-card');
		contentCard.style.width = '100%';

		const aimVisit = document.createElement('textarea');
		const descriptionVisit = document.createElement('textarea');
		const lastVisit = document.createElement('textarea');
		const hasProthesic = document.createElement('textarea');

		aimVisit.placeholder = 'Цель визита:';
		descriptionVisit.placeholder = 'Краткое описание:';
		lastVisit.placeholder = 'Последний визит:';
		hasProthesic.placeholder = 'Имеются протезы/коронки?';

		aimVisit.id = 'aimId';
		descriptionVisit.id = 'descId';
		lastVisit.id = 'lastId';
		hasProthesic.id = 'prothesicId';

		addClassStyle(aimVisit, descriptionVisit, lastVisit, hasProthesic);

		const createCard = createButton_elem();
		createCard.id = 'forDelete';

		doctorBoxData.append(aimVisit, descriptionVisit, lastVisit, hasProthesic, createCard);

		createCard.addEventListener('click', (event) => {

			event.preventDefault();

			const fioInput = document.querySelector('#fioInput');
			const ageInput = document.querySelector('#ageInput');

			const aimText = document.querySelector('#aimId');
			const descText = document.querySelector('#descId');
			const lastText = document.querySelector('#lastId');
			const prothesicText = document.querySelector('#prothesicId');

			if (fioInput.value !== '' && ageInput.value !== '' && aimText.value !== '' && descText.value !== '' && lastText.value !== '' && prothesicText.value !== '' && choosePriority.value !== 'choose a priority') {
				sendInputDataToClass(); //создаёт экземпляр класса инпут с оъектом введенных значений

				doctorBox.style.display = 'none';

				allTextAreaValue.aimVisit = aimVisit.value;
				allTextAreaValue.descriptionVisit = descriptionVisit.value;
				allTextAreaValue.lastVisit = lastVisit.value;
				allTextAreaValue.hasProsthesis = hasProthesic.value;

				const createForm = new dentistForm(allInputValue, allSelectValue, allTextAreaValue);
				//return createForm;
				deleteElem(aimText, descText, lastText, prothesicText, createCard);

				const dropDownBtn = document.querySelector('#dropdownVisitMenuButton');
				dropDownBtn.disabled = false;
			} else {
				if (fioInput.value === '') {
					fioInput.style.borderColor = 'red';
				}
				if (ageInput.value === '') {
					ageInput.style.borderColor = 'red';
				}
				if (aimText.value === '') {
					aimText.style.borderColor = 'red';
				}
				if (descText.value === '') {
					descText.style.borderColor = 'red';
				}
				if (lastText.value === '') {
					lastText.style.borderColor = 'red';
				}
				if (prothesicText.value === '') {
					prothesicText.style.borderColor = 'red';
				}
				if (choosePriority.value === 'choose a priority') {
					alert('Choose priority of order please');
				}
			}

		})

		btnClose.addEventListener('click', (event) => {
			event.preventDefault();

			doctorBox.style.display = 'none';
			searchInput.style.display = 'flex';
			dropDown.style.display = 'block';

			const aimText = document.querySelector('#aimId');
			const descText = document.querySelector('#descId');
			const lastText = document.querySelector('#lastId');
			const prothesicText = document.querySelector('#prothesicId');
			const deleteBtn = document.querySelector('#forDelete');

			deleteElem(aimText, descText, lastText, prothesicText, createCard);

			const dropDownBtn = document.querySelector('#dropdownVisitMenuButton');
			dropDownBtn.disabled = false;
		})


	}
}

export { Input, Select, TextArea }
