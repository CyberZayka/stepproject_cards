//здесь уже отрисовка готовых карточек,куда на место экзмеляров, что я ниже прописал вручную должны прилетать объекты
// с данными из формы. нужно стили добавить
import {changeCard, deleteCard} from '../requests/_requests.js';

class Visit {
	//static cardBlock = document.createElement('div');

	constructor({ fio, age, doctor, priority, aimVisit, descriptionVisit }) {
		this.fio = fio;
		this.age = age;
		this.doctor = doctor;
		this.priority = priority;
		this.aimVisit = aimVisit;
		this.descriptionVisit = descriptionVisit;
		this.cardElement = document.createElement('div');
		this.createCard();
	}

	createCard() {
		this.cardElement.classList.add('card');
		const clientFio = document.createElement('p');
		const clientAge = document.createElement('p');
		const clientDoctor = document.createElement('p');
		const clientAim = document.createElement('p');
		const clientAimDesc = document.createElement('p');
		const orderInfo = document.createElement('p');

		clientFio.dataset.field = 'fio';
		clientAge.dataset.field = 'age';
		clientDoctor.dataset.field = 'doctor';
		clientAim.dataset.field = 'aimVisit';
		clientAimDesc.dataset.field = 'descriptionVisit';
		orderInfo.dataset.field = 'priority';

		clientFio.innerText = `Имя и фамилия пациента: ${this.fio}`;
		clientAge.innerText = `Возраст пациента: ${this.age}`;
		clientDoctor.innerText = `Лечащий врач: ${this.doctor}`;
		clientAim.innerText = `Цель визита: ${this.aimVisit}`;
		clientAimDesc.innerText = `Краткое описание: ${this.descriptionVisit}`;
		orderInfo.innerText = `Срочность визита: ${this.priority}`;

		this.cardElement.append(clientFio,clientAge, clientDoctor, clientAim, clientAimDesc, orderInfo);
		this.cardElement.style.border = '5px solid black';
		this.cardElement.style.margin = '10px';
		this.cardElement.style.padding = '10px';
	}

	getId(id) {
		this.id = id;
	}

	deleteButton() {
		const deleteButton = document.createElement('button');
		deleteButton.innerText = 'Удалить карточку';
		deleteButton.addEventListener('click', (event) => {

			//Создаем и показываем лоадер
			const loading = document.createElement('div');
			loading.innerText = 'Загрузка...';
			deleteButton.after(loading);
			deleteCard(this)
				.then(response => {
					if (response.response){
						const successMes = document.createElement('div');
						successMes.innerText = `Успешно`;
						successMes.style.color = 'green';
						loading.remove();
						event.target.after(successMes);
						setTimeout( () =>{this.delete()}, 1000);
					}else{
						const errorMes = document.createElement('div');
						errorMes.innerText = `Ошибка ! ${response.error}`;
						errorMes.style.color = 'red';
						loading.remove();
						event.target.after(errorMes);
						setTimeout(() => {errorMes.remove()}, 3000);
					}
				})
		});
		return deleteButton;
	}

	changeButton() {
		const changeButton = document.createElement('button');
		changeButton.innerText = ' Редактировать карточку';
		changeButton.style.marginTop = '10px';
		changeButton.addEventListener('click', (event) => {
			changeButton.style.backgroundColor = 'green';
			//Сравниваем если ли какой-то элемент после кнопки 'редактировать'
			//Если есть - не заходим в функцию а удаляем следующий элемент ( имитация закрытия )
			if (!changeButton.nextElementSibling) {

				//Создаем и показываем пользователю подсказку
				const helpText = document.createElement('div');
				helpText.innerText = 'КЛИКНИТИЕ ПО ПОЛЯМ КАРТОЧКИ, КОТОРЫЕ НУЖНО ИЗМЕНИТЬ';
				helpText.style.cssText = 'border: 2px solid black; text-align: center; margin-top: 5px; max-width: 100%;';
				changeButton.insertAdjacentElement('afterend', helpText);
				this.cardElement.style.cursor = 'pointer';

				//Выбираем все поля с информацией в текущей карточке
				const fields = [...this.cardElement.children].filter(item => item.tagName === 'P');
				this.cardElement.addEventListener('click', (event) => {

					//Не делаем выборку если пользователь не редактирует карточку
					if (!changeButton.nextElementSibling) return ;

					//Проверяем на какое поле нажали
					if(event.target.tagName !== 'P') return ;

					//Запрещаем изменять врача
					if (event.target.dataset.field === 'doctor') {
						if (!event.target.children.length) {
							const errorText = document.createElement('div');
							errorText.innerText = 'Вы не можете изменить врача !';
							errorText.style.color = 'red';
							event.target.append(errorText);
							setTimeout(() => {
								errorText.remove();
							}, 2000);
							return;
						}
						return ;
					}


					//Сравниваем имеющиеся поля с новым кликнутым
					const inputs = helpText.querySelectorAll('input');
					let flag = false;
					inputs.forEach(item => {
						if (item.dataset.field === event.target.dataset.field) {
							flag = true;
							if (!event.target.children.length) {
								const errorBlock = document.createElement('div');
								errorBlock.innerText = 'Это поле уже редактируется !';
								errorBlock.style.color = 'red';
								event.target.insertAdjacentElement('beforeend', errorBlock);
								setTimeout(() => {
									errorBlock.remove();
								}, 3000)
							}

						}
					})

					//Если такое поле уже имеется, то дубликат не добавляем и выходим с функции
					if (flag) return ;

					//Устанавливаем поле для ввода нового значения
					const newValue = document.createElement('input');
					const textValue = event.target.innerText;
					newValue.placeholder = `${textValue.slice(0, textValue.indexOf(':'))}`;
					newValue.style.marginTop = '10px';

					//Ставим полю уникальный идентификатор
					newValue.dataset.field = event.target.dataset.field;

					//Создаем кнопку для удаления инпута для редактирования
					const deleteNewValue = document.createElement('button');
					deleteNewValue.innerText = 'X';
					deleteNewValue.style.cssText = 'background-color: red;'
					deleteNewValue.classList.add('removeField');
					deleteNewValue.addEventListener('click', function (event){
						this.previousElementSibling.remove();
						if (this.nextElementSibling.tagName === 'DIV') this.nextElementSibling.remove();
						this.remove();
						const nowInputs = helpText.querySelectorAll('input');
						if (!nowInputs.length) {
							helpText.remove();
							changeButton.style.backgroundColor = '';
						}
					})

					//Вставляем поле в конкретное место после кнопки подтверждения редактирования
					if (helpText.lastElementChild && helpText.lastElementChild.tagName === 'BUTTON'){
						helpText.lastElementChild.insertAdjacentElement('beforebegin', newValue);
					}else {
						helpText.insertAdjacentElement('beforeend', newValue);
					}

					newValue.insertAdjacentElement('afterend', deleteNewValue);

					//Создаем кнопку для подтверждения редактирования
					const setNewFields = document.createElement('button');
					setNewFields.addEventListener('click', () => {
						let check = true;
						const inputs = helpText.querySelectorAll('input');

						//Делаем валидацию  полей
						inputs.forEach(item => {
							if(item.value === '') {
								item.style.border = '2px solid red';
								const errorText = document.createElement('div');
								errorText.style.cssText = 'text-transform: uppercase; color: red;';
								errorText.innerText = 'Введите значение';
								if (item.nextElementSibling.nextElementSibling.tagName !== 'DIV') {
									item.nextElementSibling.insertAdjacentElement('afterend', errorText);
								}
								check = false;
								return;
							}
							if(item.value !== '' && item.nextElementSibling.nextElementSibling && item.nextElementSibling.nextElementSibling.tagName === 'DIV'){
								item.nextElementSibling.nextElementSibling.remove();
								item.style.cssText = 'margin-top: 5px;';
							}
							if(item.dataset.field === 'priority'){
								if (item.value !== 'high' && item.value !== 'low' && item.value !== 'normal'){
									const errorText = document.createElement('div');
									errorText.style.cssText = 'color: red;';
									errorText.innerText = 'Введите high или normal или low';
									item.nextElementSibling.insertAdjacentElement('afterend', errorText);
									check = false;
								}else if (item.nextElementSibling.tagName === 'DIV'){
									item.nextElementSibling.remove();
								}
							}
						})

						//Делаем проверку (заполнены ли все поля) перед запросом на сервер
						if (check) {

							//Делаем копию для тестовой отправки на сервер
							const testCard = {...this};

							//Обновляем новыми значениями
							inputs.forEach(item => {
								testCard[item.dataset.field] = item.value;
							})

							//Убираем все инпуты и кнопки и блоке редактирования
							while (helpText.firstChild) {
								helpText.removeChild(helpText.firstChild);
							}

							//Создаем и показываем лоадер
							const loading = document.createElement('div');
							loading.innerText = 'Загрузка...';
							helpText.append(loading);


							changeCard(this.id, testCard)
								.then(response => {

									//При успешном ответе
									if (response.response){

										//Делаем сообщение об успехе редактирования
										const success = document.createElement('div');
										success.innerText = 'Данные обновлены';
										success.style.color= 'green';

										//Убираем лоадер
										loading.remove();

										//Показываем сообщение об успехе
										helpText.append(success);

										inputs.forEach(item => {
											//Обновляем значение в объекте каточки
											this[item.dataset.field] = item.value;

											//Находим элемент значения которого нужно изменить и изменяем его
											const neededField = fields.find(element => element.dataset.field === item.dataset.field);
											neededField.innerText = neededField.innerText.slice(0, neededField.innerText.indexOf(':') + 1) + " " + this[item.dataset.field];
										})
										setTimeout(() => {helpText.remove()}, 2000);
										changeButton.style.backgroundColor = '';
									}else{
										//Создаем и показываем сообщение об ошибке
										const error = document.createElement('div');
										error.innerText = `Данные не обновлены по причине ${response.error}`;
										error.style.color = 'red';
										helpText.append(error);

										//Убираем лоадер
										loading.remove();

										setTimeout(() => {helpText.remove()}, 5000);
										changeButton.style.backgroundColor = '';
									}
								})

						}
					})

					setNewFields.innerText = 'Установить новые значения';
					setNewFields.style.cssText = 'display: block; margin: 0 auto; margin-top: 10px';

					//Проверяем если кнопка подтверждения редактирования уже есть - не создаем её
					if (!helpText.querySelectorAll('button:not(.removeField)').length) helpText.append(setNewFields);
				});

			}else{
				changeButton.style.backgroundColor = '';
				this.cardElement.style.cursor = '';
				changeButton.nextElementSibling.remove();
			}
		});
		return changeButton;
	}
	delete() {
		this.cardElement.remove();
	}
}

class VisitDentist extends Visit {
	constructor({ fio, age, doctor, priority, aimVisit, descriptionVisit, lastVisit, hasProsthesis }) {
		super({ fio, age, doctor, priority, aimVisit, descriptionVisit });
		this.lastVisit = lastVisit;
		this.hasProsthesis = hasProsthesis;
	}

	showCard() {
		const lastVisitInfo = document.createElement('p');
		const hasProsthesisInfo = document.createElement('p');
		const cards = document.querySelector('.cards');

		lastVisitInfo.innerText = `Последний визит: ${this.lastVisit}`;
		hasProsthesisInfo.innerText = `Наличие коронок/протезов: ${this.hasProsthesis}`;

		lastVisitInfo.dataset.field = 'lastVisit';
		hasProsthesisInfo.dataset.field = 'hasProsthesis';

		this.cardElement.append(lastVisitInfo, hasProsthesisInfo, this.deleteButton(), this.changeButton());
		cards.insertAdjacentElement('afterbegin', this.cardElement);
	}
}

class VisitCardiologist extends Visit {
	constructor({ fio, age, doctor, priority, aimVisit, descriptionVisit, pressure, massIndex, heartAche }) {
		super({ fio, age, doctor, priority, aimVisit, descriptionVisit });
		this.pressure = pressure;
		this.massIndex = massIndex;
		this.heartAche = heartAche;
	}

	showCard() {
		const pressureInfo = document.createElement('p');
		const massIndexInfo = document.createElement('p');
		const heartAcheInfo = document.createElement('p');
		const cards = document.querySelector('.cards');

		pressureInfo.innerText = `Обычное давление пациента: ${this.pressure}`;
		massIndexInfo.innerText = `Индекс массы тела: ${this.massIndex}`;
		heartAcheInfo.innerText = `Перенесённые заболевания сердечно-сосудистой системы: ${this.heartAche}`;

		pressureInfo.dataset.field = 'pressure';
		massIndexInfo.dataset.field = 'massIndex';
		heartAcheInfo.dataset.field = 'heartAche';

		this.cardElement.append(pressureInfo, massIndexInfo, heartAcheInfo, this.deleteButton(), this.changeButton());
		cards.insertAdjacentElement('afterbegin',this.cardElement);
	}
}

class VisitTherapist extends Visit {
	showCard() {
		const cards = document.querySelector('.cards');

		this.cardElement.append(this.deleteButton(), this.changeButton());
		cards.insertAdjacentElement('afterbegin',this.cardElement);
	}
}

export { Visit, VisitCardiologist, VisitDentist, VisitTherapist };
