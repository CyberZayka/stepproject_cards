import {VisitCardiologist, VisitDentist, VisitTherapist} from "./visitClasses.js";
import {createCard} from "../requests/_requests.js";
import {allCards} from "../../../_classes.js";

class Form {

    constructor(input, select, textArea) {
        this.input = input;
        this.select = select;
        this.textArea = textArea;
    }


    sendDoctorsData(cardio, dent, therapic) {
        if (cardio) {
            const visitCard = new VisitCardiologist(cardio);
            createCard(visitCard)
                .then(response => {
                    if (response.response){
                        visitCard.showCard();
                        visitCard.getId(response.data.id);
                    }else{
                        alert(`Error ! ${response.error}`);
                    }
                });
            allCards.push(visitCard);
            console.log(allCards);
        } else if (dent) {
            const visitCard = new VisitDentist(dent);
            createCard(visitCard)
                .then(response => {
                    if (response.response){
                        visitCard.showCard();
                        visitCard.getId(response.data.id);
                    }else{
                        alert(`Error ! ${response.error}`);
                    }
                });
            allCards.push(visitCard);
            console.log(allCards);
        } else if (therapic) {
            const visitCard = new VisitTherapist(therapic);
            createCard(visitCard)
                .then(response => {
                    if (response.response){
                        visitCard.showCard();
                        visitCard.getId(response.data.id);
                    }else{
                        alert(`Error ! ${response.error}`);
                    }
                });
            allCards.push(visitCard);
        }
    }

}

class cardiologistForm extends Form {
    constructor(input, select, textArea) {
        super(input, select, textArea);
        this.createCardioData();
    }


    createCardioData() {
        const dataPacientForm = Object.assign(this.input, this.select, this.textArea);

        this.sendDoctorsData(dataPacientForm);



    }
}

class dentistForm extends Form {
    constructor(input, select, textArea) {
        super(input, select, textArea);
        this.createDentistData();
    }

    createDentistData() {
        const dataPacientForm = Object.assign(this.input, this.select, this.textArea);

        this.sendDoctorsData(null,dataPacientForm);

        localStorage.setItem(`DentistCard${Object.keys(localStorage).length}`, JSON.stringify(dataPacientForm));
        localStorage.getItem(`DentistCard${Object.keys(localStorage).length}`);

    }
}

class therapistForm extends Form {
    constructor(input, select, textArea) {
        super(input, select, textArea);
        this.createTherapistData();
    }

    createTherapistData() {
        const dataPacientForm = Object.assign(this.input, this.select, this.textArea);

        this.sendDoctorsData(null,null,dataPacientForm);

        localStorage.setItem(`therapistCard${Object.keys(localStorage).length}`, JSON.stringify(dataPacientForm));
        localStorage.getItem(`therapistCard${Object.keys(localStorage).length}`);

    }
}

export {Form, cardiologistForm, dentistForm, therapistForm};